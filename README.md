Space Bar
=========

Space Bar is a stupid project to create maps in the vein of the excellently documented generator by [mewo2][mewo2].

Is It Good?
-----------

Not yet.

License
-------

```
               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                       Version 2, December 2004

    Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

    Everyone is permitted to copy and distribute verbatim or modified
    copies of this license document, and changing it is allowed as long
    as the name is changed.

               DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE

      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

     0. You just DO WHAT THE FUCK YOU WANT TO.
```

[mewo2]: https://mewo2.com/notes/terrain/